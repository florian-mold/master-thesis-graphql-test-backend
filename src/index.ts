import { startStandaloneServer } from "@apollo/server/standalone";
import { addMocksToSchema } from "@graphql-tools/mock";
import { makeExecutableSchema } from "@graphql-tools/schema";
import { ApolloServer } from "@apollo/server";
import fs, { readFileSync } from "fs";
import path from "path";
import { IResolvers } from "@graphql-tools/utils";

/**
 * Fetches all mock data from the mock data files.
 */
export const getMockData = () => ({
  ...(JSON.parse(fs.readFileSync("./src/mock-data/contact-mock-data.json", "utf-8"))),
  ...(JSON.parse(fs.readFileSync("./src/mock-data/sales-mock-data.json", "utf-8"))),
  ...(JSON.parse(fs.readFileSync("./src/mock-data/shared-mock-data.json", "utf-8"))),
});

/**
 * Load the schema from the file system.
 */
const customTypeDefs = readFileSync(
  path.join(path.resolve(), "./src/schema.graphql"),
  "utf-8",
);

const data = {
  ...getMockData(),
};

export const CUSTOM_RESOLVERS: IResolvers<any, any> | Array<IResolvers<any, any>> = {
  Query: {
    userByCredentials: (_, { email, password }) => {
      return data["users"]
        .find(
          (user: { email: string; password: string }) => user.email === email && user.password === password,
        );
    },

    userByToken: (_, { token }) => {
      return data["users"]
        .find(
          (user: { token: string }) => user.token === token,
        );
    },

    allSalutations: () => data["salutations"],
    allTitles: () => data["titles"],
    allCountries: () => data["countries"],
    allAddresses: () => data["addresses"],

    Contact: (_, { id }) => {
      return data["contacts"]
        .find(
          (contact: { id: string }) => contact.id === id,
        );
    },
    allContacts: (_, { page, perPage }) => {
      const contacts: any[] = data["contacts"];

      if (!page && !perPage) {
        return contacts;
      }

      const start = page * perPage;
      const end = page * perPage + perPage + 1;
      return contacts.slice(start, end);
    },
    _allContactsMeta: () => {
      const contacts = data["contacts"];
      return {
        count: contacts.length,
      };
    },

    allEmailTypes: () => data["emailTypes"],
    allPeople: () => data["people"],
    allDirectDebits: () => data["directDebits"],
    allPaypalAccounts: () => data["paypalAccounts"],
    allPaymentMethods: () => data["paymentMethods"],
    allArticleUnits: () => data["articleUnits"],
    allCurrencies: () => data["currencies"],
    allSalesPaymentMethods: () => data["salesPaymentMethods"],
    allVats: () => data["vats"],
    allArticles: () => data["articles"],

    Contract: (_, { id }) => {
      return data["contracts"]
        .find(
          (contract: { id: string }) => contract.id === id,
        );
    },
    allContracts: (_, { page, perPage }) => {
      const contracts: any[] = data["contracts"];

      if (!page && !perPage) {
        return contracts;
      }

      const start = page * perPage;
      const end = page * perPage + perPage + 1;
      return contracts.slice(start, end);
    },
    _allContractsMeta: () => {
      const contacts = data["contracts"];
      return {
        count: contacts.length,
      };
    },

    Invoice: (_, { id }) => {
      return data["invoices"]
        .find(
          (invoice: { id: string }) => invoice.id === id,
        );
    },
    allInvoices: (_, { page, perPage }) => {
      const invoices: any[] = data["invoices"];

      if (!page && !perPage) {
        return invoices;
      }

      const start = page * perPage;
      const end = page * perPage + perPage + 1;
      return invoices.slice(start, end);
    },
    _allInvoicesMeta: () => {
      const invoices = data["invoices"];
      return {
        count: invoices.length,
      };
    },

    allInvoiceTypes: () => data["invoiceTypes"],

    User: (_, { id }) => {
      return data["users"]
        .find(
          (user: { id: string }) => user.id === id,
        );
    },
    allUsers: (_, { page, perPage }) => {
      const users: any[] = data["users"];

      if (!page && !perPage) {
        return users;
      }

      const start = page * perPage;
      const end = page * perPage + perPage + 1;
      return users.slice(start, end);
    },
    _allUsersMeta: () => {
      const users = data["users"];
      return {
        count: users.length,
      };
    },
  },
  Invoice: {
    CreatorClient(parent) {
      return data["creatorClients"].find((client: { id: string }) => client.id === parent.creatorClient_id);
    },
    ModifyClient(parent) {
      return data["modifyClients"].find((client: { id: string }) => client.id === parent.modifyClient_id);
    },
    Customer(parent) {
      return data["customers"].find((customer: { id: string }) => customer.id === parent.customer_id);
    },
    InvoiceType(parent) {
      return data["invoiceTypes"].find((invoiceType: { id: string }) => invoiceType.id === parent.invoiceType_id);
    },
    SalesPaymentMethod(parent) {
      return data["salesPaymentMethods"].find((salesPaymentMethod: { id: string }) => salesPaymentMethod.id === parent.sales_payment_method_id);
    },
    InvoicePositions(parent) {
      return data["invoicePositions"].filter((invoicePosition: { invoice_id: string }) => invoicePosition.invoice_id === parent.id);
    },
    SentInvoices(parent) {
      return data["sentInvoices"].filter((sentInvoice: { invoice_id: string }) => sentInvoice.invoice_id === parent.id);
    },
    Notes(parent) {
      return data["notes"].filter((note: { invoice_id: string }) => note.invoice_id === parent.id);
    },
  },
  SentInvoice: {
    Invoice(parent) {
      return data["invoices"].find((invoice: { id: string }) => invoice.id === parent.invoice_id);
    },
  },
  CreatorClient: {
    Client(parent) {
      return data["clients"].find((client: { id: string }) => client.id === parent.client_id);
    },
  },
  ModifyClient: {
    Client(parent) {
      return data["clients"].find((client: { id: string }) => client.id === parent.client_id);
    },
  },
  Currency: {
    Country(parent) {
      return data["countries"].find((country: { id: string }) => country.id === parent.country_id);
    },
  },
  Customer: {
    Country(parent) {
      return data["countries"].find((country: { id: string }) => country.id === parent.country_id);
    },
  },
  Article: {
    ArticleUnit(parent) {
      return data["articleUnits"].find((articleUnit: { id: string }) => articleUnit.id === parent.articleUnit_id);
    },
    CreatorClient(parent) {
      return data["creatorClients"].find((client: { id: string }) => client.id === parent.creatorClient_id);
    },
    ModifyClient(parent) {
      return data["modifyClients"].find((client: { id: string }) => client.id === parent.modifyClient_id);
    },
  },
  InvoicePosition: {
    Article(parent) {
      return data["articles"].find((article: { id: string }) => article.id === parent.article_id);
    },
    Contract(parent) {
      return data["contracts"].find((contract: { id: string }) => contract.id === parent.contract_id);
    },
    Vat(parent) {
      return data["vats"].find((vat: { id: string }) => vat.id === parent.vat_id);
    },
  },
  Contract: {
    Client(parent) {
      return data["clients"].find((client: { id: string }) => client.id === parent.client_id);
    },
    Customer(parent) {
      return data["customers"].find((customer: { id: string }) => customer.id === parent.customer_id);
    },
  },
  Contact: {
    Title(parent) {
      return data["titles"].find((title: { id: string }) => title.id === parent.title_id);
    },
    Salutation(parent) {
      return data["salutations"].find((salutation: { id: string }) => salutation.id === parent.salutation_id);
    },
    Address(parent) {
      return data["addresses"].find((address: { id: string }) => address.id === parent.address_id);
    },
    PaymentMethods(parent) {
      return data["paymentMethods"].filter((paymentMethod: { contact_id: string }) => paymentMethod.contact_id === parent.id);
    },
    SubContacts(parent) {
      return data["subContacts"].filter((subContact: { contact_id: string }) => subContact.contact_id === parent.id);
    },
  },
  User: {
    Title(parent) {
      return data["titles"].find((title: { id: string }) => title.id === parent.title_id);
    },
    Salutation(parent) {
      return data["salutations"].find((title: { id: string }) => title.id === parent.salutation_id);
    },
    Address(parent) {
      return data["addresses"].find((address: { id: string }) => address.id === parent.address_id);
    },
  },
  Address: {
    Country(parent) {
      return data["countries"].find((country: { id: string }) => country.id === parent.country_id);
    },
  },
  SubContact: {
    Person(parent) {
      return data["people"].find((person: { id: string }) => person.id === parent.person_id);
    },
    EmailType(parent) {
      return data["emailTypes"].find((emailType: { id: string }) => emailType.id === parent.emailType_id);
    },
  },
};

const schema = makeExecutableSchema({
  typeDefs: [
    customTypeDefs,
  ],
  resolvers: {
    ...CUSTOM_RESOLVERS,
  },
});

addMocksToSchema({
  schema,
  mocks: {},
});


/**
 * Startup the server
 *
 * @returns The url of the server.
 */
const main = async () => {
  const server = new ApolloServer({ schema, introspection: true });
  return await startStandaloneServer(server, { listen: { port: 3000 } });
};

main()
  .then(({ url }) => console.log(`🚀  Server ready at: ${url}`))
  .catch(console.log);
